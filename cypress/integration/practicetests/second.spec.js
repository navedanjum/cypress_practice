
/// <reference types="Cypress" />

describe('Second test first block', () => {
  before('Before hook first', () => {
    console.log('Before block: I will run only once before all tests in first block');

  });

  beforeEach('BeforeEach hook', () => {
    console.log('BeforeEach block: I will run before each and every test in the first block');
  });

  after('After hook first', () => {
    console.log('After block: I will run only once after all tests in first block')
  });

  afterEach('AfterEach hook', () => {
    console.log('AfterEach block: I will run after each and every test in the first block');
  });


  it('visit clarifai website', () => {
    cy.visit("https://www.clarifai.com").
      get('#hs-eu-confirmation-button').click();
    console.log('Visited clarifai home page');

  });

  it('Do nothing', () => {
    console.log('Do nothing block');

  });


})

describe('Multiple it blocks', () => {

  before('Before hook second', () => {
    console.log('Before block: I will run only once before all tests in second block');
  });

  beforeEach('BeforeEach hook', () => {
    console.log('BeforeEach block: I will run before each and every test in the second block');
  });

  after('After hook second', () => {
    console.log('After block: I will run only once after all tests in second block');
  });

  afterEach('AfterEach hook', () => {
    console.log('AfterEach block: I will run after each and every test in the second block');
  });


  it('block1', () => {
    console.log('I am in block 1');

  });

  it('block2', () => {
    console.log('I am in block 2');

  });

  it('block3', () => {
    console.log('I am in block 3');

  });

})