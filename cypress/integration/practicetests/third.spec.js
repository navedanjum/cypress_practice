/// <reference types="cypress" />

/*
basic test to login to portal and search for app with keyword 
*/


describe('Search App', () => {
    it('Portal login',() => {

        cy.visit('https://developer-dev.clarifai.com');
        cy.get('#login-email').type('fe@clarifai.com');
        cy.get('#login-password').type("**********");
        cy.get('#login-submit').click();

        //cy.wait(6000); //Loading apps
        cy.get("input[placeholder *='search' i]").type('zaur', {force: true});

        cy.get('a[data-testid*="zaur" i]').as('searchResult');  //Aliasing
        cy.get('@searchResult').should('have.length',5);

        cy.get('@searchResult').eq(3).click();

        //log out 
        cy.get('[data-testid=button-leftNavProfileIcon]').click();
        cy.get(':nth-child(10) > .menu-item-link').click();

    })
})