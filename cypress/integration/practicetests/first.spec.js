
/// <reference types="Cypress" />

describe('My First Test', () => {
  it('visit clarifai website', () => {
    cy.visit("https://www.clarifai.com").
      get('#hs-eu-confirmation-button').click();

  })
})

describe('Multiple it blocks', () => {
  it('block1', () => {
    console.log('I am in block 1');

  })

  it('block2', () => {
    console.log('I am in block 2');

  })

  it('block3', () => {
    console.log('I am in block 3');

  })

})



